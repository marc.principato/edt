package com.example.edt_test;
//package android;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private TextView theDate;
    private Button btnGoCalender;
    private Button btnGoDaily;

    @Override
    protected void onCreate(Bundle SaveInstanceState) {
        super.onCreate(SaveInstanceState);
        setContentView(R.layout.activity_main);
        new FetchTask().execute("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN");



    }

    public class FetchTask extends AsyncTask<String, Void, ArrayList<ArrayList>>{
        @Override
        protected ArrayList<ArrayList> doInBackground(String... urls) {
            InputStream inputStream = null;
            HttpURLConnection conn = null;

            String stringUrl = urls[0];
            try {
                URL url = new URL(stringUrl);
                conn = (HttpURLConnection) url.openConnection();
                conn.connect();
                int response = conn.getResponseCode();
                if (response != 200) {
                    return null;
                }

                inputStream = conn.getInputStream();
                if (inputStream == null) {
                    return null;
                }

                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
                BufferedReader reader = new BufferedReader(inputStreamReader);
                String st;
                String test= null;
                String[] sti;
                String[] sto;
                ArrayList tab_ddc = new ArrayList();
                ArrayList tab_dfc = new ArrayList();
                ArrayList tab_salle = new ArrayList();
                ArrayList tab_matiere = new ArrayList();
                ArrayList tab_professeur = new ArrayList();
                ArrayList tab_groupe = new ArrayList();
                ArrayList tab_type_cours = new ArrayList();
                String date_debut_cours= null;
                String date_fin_cours= null;
                String heure_fin_cours= null;
                String heure_debut_cours= null;
                String professeur= null;
                String matiere= null;
                String type_cours= null;
                String groupe= null;
                String salle= null;
                int taille;
                ArrayList Cours_tab = new ArrayList();
                String Cours = "";


                while ((st = reader.readLine()) != null) {
                    if (st.contains("DTSTART")) {
                        sti = st.split(":");
                        test = sti[1];
                        sti = test.split("T");
                        date_debut_cours = sti[0];
                        //heure_debut_cours = sti[1];
                        tab_ddc.add(date_debut_cours);
                    }
                    if (st.contains("DTEND")){
                        sti = st.split(":");
                        test = sti[1];
                        sti = test.split("T");
                        date_fin_cours = sti[0];
                        //heure_fin_cours = sti[1];
                        tab_dfc.add(date_fin_cours);
                    }
                    if (st.contains("LOCATION;LANGUAGE=fr:")){
                        sti = st.split(":");
                        salle = sti[1];
                        tab_salle.add(salle);
                    }
                    if (st.contains("SUMMARY;LANGUAGE")) {
                        sti = st.split(":");
                        test = sti[1];
                        sto = test.split("-");
                        taille = sto.length;
                        if (taille >= 1) {
                            matiere = sto[0];
                            tab_matiere.add(matiere);
                        }
                        if (taille >= 2) {
                            professeur = sto[1].substring(1);
                            tab_professeur.add(professeur);
                        }
                        if (taille >= 3) {
                            groupe = sto[2].substring(1);
                            tab_groupe.add(groupe);
                        }
                        if (taille >= 4) {
                            type_cours = sto[3].substring(1);
                            tab_type_cours.add(type_cours);
                        }
                    }
                }

                Cours_tab.add(tab_ddc);
                Cours_tab.add(tab_dfc);
                Cours_tab.add(tab_salle);
                Cours_tab.add(tab_matiere);
                Cours_tab.add(tab_professeur);
                Cours_tab.add(tab_groupe);
                Cours_tab.add(tab_type_cours);



                //return new String(buffer);
                return (Cours_tab);
            } catch (IOException e) {
                return null;
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }

        @Override
        protected void onPostExecute(ArrayList<ArrayList> s)
        {

            setContentView(R.layout.activity_main);
            TextView tv = findViewById(R.id.TextView1);
            //tv.setText((String) s.get(5).get(650));
            super.onPostExecute(s);
            theDate = findViewById(R.id.date);
            btnGoCalender = findViewById(R.id.btnGoCalender);

            Intent incommingIntent = getIntent();
            String date = incommingIntent.getStringExtra("date");
            //theDate.setText(date);


            btnGoCalender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, CalendarActivity.class);
                    startActivity(intent);
                }
            });

            btnGoDaily = findViewById(R.id.btnGodaily);
            btnGoDaily.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, recyclerview.class);
                    startActivity(intent);
                }
            });
            if (s == null) {
                //str = "erreur";
                //MajListView(null,str,null,str,0,str);
            } else {
                //func(s);
            }

        }
    }}
