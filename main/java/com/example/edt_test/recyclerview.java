package com.example.edt_test;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class recyclerview extends AppCompatActivity {

    ListView mListView;


    @Override
    protected void onCreate(Bundle savecInstancesState){
        super.onCreate(savecInstancesState);
        setContentView(R.layout.recyclerview_layout);

        mListView = findViewById(R.id.listView);
        CustomAdaptor customAdaptor = new CustomAdaptor();
        mListView.setAdapter(customAdaptor);

    }
    class CustomAdaptor extends BaseAdapter{
        String[] prof={"MERLIN","HUET","REY"};
        String[] Cours={"APPLI MOBILE","APPLI MOBILE","ANGLAIS"};
        String[] groupe={"L3INFO_TD2","L3INFO_TD2","L3INFO_TD2"};
        String[] heure_debut={"8h30","11h30","14h30"};
        String[] heure_fin={"11h30","13h00","16h00"};
        String[] type_cours={"TD","CM","TD"};




        @Override
        public int getCount() {
            return prof.length;


        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.item_files, null);

            TextView mTextView_cours = view.findViewById(R.id.cours);
            TextView mTextView_prof = view.findViewById(R.id.prof);
            TextView mTextView_groupe = view.findViewById(R.id.groupe);
            TextView mTextView_debut = view.findViewById(R.id.heure_debut);
            TextView mTextView_fin = view.findViewById(R.id.heure_fin);
            TextView mTextView_type_cours = view.findViewById(R.id.type_cours);

            mTextView_cours.setText(Cours[position]);
            mTextView_prof.setText(prof[position]);
            mTextView_groupe.setText(groupe[position]);
            mTextView_debut.setText(heure_debut[position]);
            mTextView_fin.setText(heure_fin[position]);
            mTextView_type_cours.setText(type_cours[position]);

            return view;
        }
    }
}
